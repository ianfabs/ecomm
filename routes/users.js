var express = require('express');
var router = express.Router();

const knex = require('knex')(
  {
    client: 'mssql',
    connection: {
      host : 'sql.neit.edu',
      port : 4500,
      user : '001416358',
      password : '001416358',
      database : 'SE41781_IFabricatore',
    }
  }
);

/* GET users listing. */
router.post('/login', async (req, res, next) => {
  const _u = {...req.body};
  const user = await knex
    .select('handle', 'name', 'logins')
    .from("Users")
    .where('handle', '=', _u.handle)
    .andWhere('password', '=', _u.password);
  if(user.length > 0 && user.length < 2) {
    let _user = await knex("Users")
    .where('handle', '=', _u.handle)
    .andWhere('password', '=', _u.password)
    .update({
      logins: user[0].logins+1,
    });
    req.session.user = _user[0];
    res.end("Login success");
  }
  else res.end("Failed to login");
});

router.post('/register', async (req, res, next) => {
  console.log(req.body);
  const {name, handle, password} = req.body;
  const user = await knex("Users").insert({name, handle, password, logins: 0});
  res.end();
});

module.exports = router;
